void main() {
  // we are assigning our String Variables from no1-10 and we will print them below
  String winnerno1 =
      "1)These were the winners of MTN app of the year awards in 2012";
  String winnerno2 =
      "2)These were the winners of MTN app of the year awards in 2013";
  String winnerno3 =
      "3)These were the winners of MTN app of the year awards in 2014";
  String winnerno4 =
      "4)These were the winners of MTN app of the year awards in 2015";
  String winnerno5 =
      "5)These were the winners of MTN app of the year awards in 2016";
  String winnerno6 =
      "6)These were the winners of MTN app of the year awards in 2017";
  String winnerno7 =
      "7)These were the winners of MTN app of the year awards in 2018";
  String winnerno8 =
      "8)These were the winners of MTN app of the year awards in 2019";
  String winnerno9 =
      "9)These were the winners of MTN app of the year awards in 2020";
  String winnerno10 =
      "10)These were the winners of MTN app of the year awards in 2021";

// This is a list collection of all the apps that won the MTN App of the year awards from 2012 to 2021
  List winnersnoone = [
    'FNB Banking App',
    'Discovery Health ID',
    'Plascon',
    'Rapid Targets',
    'ZA',
    'MM',
  ];
  List winnersnotwo = [
    'Nedbank',
    'Bookly',
    'DSTV',
    'Pricecheck',
    'Deloite Digital',
    'Gautrain buddy',
    'Kids Aid',
    'Markitshare',
    'Snapscan',
  ];
  List winnersnothree = [
    'Supersport',
    'Zapper',
    'Sync mobile',
    'Liveinspect',
    'my-belongings',
    'Reavaya',
    'Vigo',
    'Wildlife-tracker',
  ];
  List winnersnofour = [
    'DSTV Now App',
    'M4JAM',
    'WumDrop',
    'Vula mobile',
    'Eskomsepush',
    'CPUT mobile',
  ];
  List winnersnofive = [
    'HearZa',
    'M4 Jam',
    'Tuta-me',
    'Domestly',
    'Friendly-math-monstersfor-kindergarten',
    'Kaching',
    'mibrand',
  ];
  List winnersnosix = [
    'Orderin',
    'Shyft',
    'Hey Jude',
    'Zulzi',
    'Oru Social',
    'Awethu-Project',
    'Eco-Slips',
    'integreatmev',
    'Pick n Pay superanimals',
    'watif-health-portal',
    'transunion-1check'
  ];
  List winnersnoseven = [
    'Cowa-bunga',
    'Pineapple',
    'Besmarta.com',
    'African Snakebite',
    'acgl',
    'xander-english',
    'ctrl',
    'bestee',
    'Stockfella',
  ];
  List winnersnoeight = [
    'Digger',
    'Vula',
    'Hydra',
    'Loot Defence',
    'Naked Insurance',
    'over',
    'loctransi',
    'mowash',
    'My Pregnant Journey',
    'Si-realities',
    'matric-live',
    'franc',
  ];
  List winnersnonine = [
    'Easy-equities',
    'Checkers 60-60',
    'Birdpro',
    'technishen-1',
    'matric-live',
    'Stockfella',
    'bottles',
    'lexie-hearing',
    'league of legends',
    'examsta',
    'Xitsonga-dictionary',
    'Greenfingers-mobile',
    'Guardian-health',
    'My Pregnancy Journey',
  ];
  List winnersnoten = [
    'Ambani-Afrika',
    'Takealot',
    'Shyft',
    'Iidentifii',
    'SISA',
    'Guardian-Health Platform',
    'Murimi',
    'Uniwise',
    'Kazi-App',
    'Rekindle',
    'Hellopay-SoftPOS',
    'Roadsave',
  ];

// Here were sorting the list we have collected alphabetically and we are printing the assigned string var & list
  winnersnoone.sort();
  print(winnerno1);
  print(winnersnoone);

  winnersnotwo.sort();
  print(winnerno2);
  print(winnersnotwo);

  winnersnothree.sort();
  print(winnerno3);
  print(winnersnothree);

  winnersnofour.sort();
  print(winnerno4);
  print(winnersnofour);

  winnersnofive.sort();
  print(winnerno5);
  print(winnersnofive);

  winnersnosix.sort();
  print(winnerno6);
  print(winnersnosix);

  winnersnoseven.sort();
  print(winnerno7);
  print(winnersnoseven);

  winnersnoeight.sort();
  print(winnerno8);
  print(winnersnoeight);

  winnersnonine.sort();
  print(winnerno9);
  print(winnersnonine);

  winnersnoten.sort();
  print(winnerno10);
  print(winnersnoten);

// Here we are printing the winning app in 2017 & 2018
  print("The app that won the Mtn app of the year in 2017 is");
  print(winnersnosix[6]);
  print("The app that won the Mtn app of the year in 2018 is");
  print(winnersnoseven[3]);

  // This is the total number of apps that won the Mtnapp of the year awards from 2012-2021
  var sum = winnersnoone +
      winnersnotwo +
      winnersnothree +
      winnersnofour +
      winnersnofive +
      winnersnosix +
      winnersnoseven +
      winnersnoeight +
      winnersnonine +
      winnersnoten;
  print(
      'The total number of apps that won the Mtnapp of the year awards from 2012-2021 are:');
  print(sum.length);
}
